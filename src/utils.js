/* eslint-disable */

export default {
  errors (desc = null) {
    if (desc) {
      this.notify(desc)
    }
  },
  focus (selector) {
    setTimeout(() => {
      document.querySelector(selector).focus()
    }, 100)
  },
  select (selector) {
    return document.querySelector(selector)
  },
  notify (body) {
    new Notification('مهام', {
      icon: `/static/logo-64.png`,
      body,
      dir: 'rtl'
    })
  }
}
