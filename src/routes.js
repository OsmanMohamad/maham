import Stories from 'views/stories'
import Projects from 'views/projects'
import StoryDetails from 'views/story-details'

export default {
  '/': {
    component: Projects
  },
  '/p/:title': {
    name: 'stories-board',
    component: Stories
  },
  '/story/:id': {
    name: 'story-details',
    component: StoryDetails
  }
}
