
export default class Timer {

  constructor (vueComponent, counter = 0) {
    this.counter = counter
    this.vueComponent = vueComponent
    this.period = 1000
    this.timer = null
  }

  countUp () {
    this.timer = setTimeout(() => {
      this.counter += 1
      this.vueComponent.$dispatch('timer-count-up', this.counter)
      this.countUp()
    }, this.period)
  }

  start () {
    this.countUp()
  }

  stop () {
    if (this.counter > 0) {
      clearTimeout(this.timer)
      this.vueComponent.$dispatch('timer-stop')
      this.counter = 0
    }
  }
}
