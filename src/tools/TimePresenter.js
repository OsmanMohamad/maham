export default class TimePresenter {
  static decorateMinutes (counter) {
    let mins = Math.floor(counter / 60)
    let seconds = counter % 60

    if (counter >= 60) {
      return `${TimePresenter.digitsPresenter(mins)} : ${TimePresenter.digitsPresenter(seconds)}`
    }
    return `00 : ${TimePresenter.digitsPresenter(seconds)}`
  }

  static digitsPresenter (counter) {
    return (counter >= 10) ? counter : `0${counter}`
  }
}
