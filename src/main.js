import Vue from 'vue'
import VueRouter from 'vue-router'
// css
import 'static/css/fonts.css'
import 'static/css/arabic-fonts.css'
import 'static/css/keen-ui.min.css'
import 'flexboxgrid/css/flexboxgrid.css'
import 'assets/app.styl'

import Routes from './routes'

import App from 'views/App'

Vue.use(VueRouter)

const router = new VueRouter({
  history: true,
  linkActiveClass: 'active'
})

router.map(Routes)

router.start(App, 'main')
