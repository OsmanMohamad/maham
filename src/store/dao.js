import path from 'path'
import {remote} from 'electron'
import Datastore from 'nedb'
import seeds from 'src/store/seeds.json'
import utils from 'src/utils'

const storiesStorage = path.join(remote.app.getPath('userData'), 'stories.db')
const projectsStorage = path.join(remote.app.getPath('userData'), 'projects.db')

const stories = new Datastore({ filename: storiesStorage, autoload: true })
const projects = new Datastore({ filename: projectsStorage, autoload: true })

// Using a unique constraint with the index
projects.ensureIndex({ fieldName: 'title', unique: true, required: true }, function (err) {
  utils.errors(err)
})

// [{key,array[{key,array[]}]}]
// [
//   {
//     _id: 34567456,
//     content: "lorem10",
//     tasks: [
//       { details: "lorem5", active: false, criteria: 1 }
//     ],
//     criterias: [
//       { title: "some title" }
//     ]
//   }
// ]
export default {
  log (key = null) {
    stories.find({}, (err, docs) => {
      if (err) console.error(err)
      console.dir(docs)
    })
  },
  seed () {
    stories.insert(JSON.parse(seeds), (err, docs) => {
      if (err) console.error(err)
      console.log('finished seeding')
    })
  },
  saveCriteria (id, data, callback) {
    stories.update({_id: id}, {
      $push: {acceptanceCriteries: data}
    }, {returnUpdatedDocs: true}, callback)
  },
  saveTask (criteria, task, callback) {
    let query = {'acceptanceCriteries.description': criteria.description}
    let update = {}
    update[`acceptanceCriteries.${criteria.id}.tasks`] = task
    stories.update(query, {$push: update}, {returnUpdatedDocs: true}, callback)
  },
  toggleTaskActivation (data, callback) {
    // set document id
    let query = {_id: data._id}
    // prepare update statement
    let update = {$set: {}}
    let key = `acceptanceCriteries.${data.index}.tasks.${data.key}.active`
    update.$set[key] = data.active
    // run query and return the affected doc
    stories.update(query, update, {returnUpdatedDocs: true}, callback)
  },
  saveTaskCounter (counter, data, callback) {
    // set document id
    let query = {_id: data._id}
    // prepare update statement
    let update = {$set: {}}
    let key = `acceptanceCriteries.${data.index}.tasks.${data.key}.timePeriod`
    update.$set[key] = counter
    // run query and return the affected doc
    stories.update(query, update, {}, callback)
  },
  updateTaskDetails (details, data, callback) {
    // set document id
    let query = {_id: data._id}
    // prepare update statement
    let update = {$set: {}}
    let key = `acceptanceCriteries.${data.index}.tasks.${data.key}.details`
    update.$set[key] = details
    // run query and return the affected doc
    stories.update(query, update, {}, callback)
  },
  updateCriteriaDetails (details, data, callback) {
    // set document id
    let query = {_id: data._id}
    // prepare update statement
    let update = {$set: {}}
    let key = `acceptanceCriteries.${data.index}.description`
    update.$set[key] = details
    // run query and return the affected doc
    stories.update(query, update, {}, callback)
  },
  removeTask (keys, callback) {
    let query = {_id: keys.storyId}
    let update = {}
    // set array path to search in it
    let key = `acceptanceCriteries.${keys.criteriaIndex}.tasks`
    // prepare puller
    update['$pull'] = {}
    // find by all object to remove by it
    update.$pull[key] = keys.task
    // run the query
    stories.update(query, update, {}, callback)
  },
  removeCriteria (storyId, criteria, callback) {
    let query = {_id: storyId}
    let update = {}
    // set array path to search in it
    let key = `acceptanceCriteries`
    // prepare puller
    update['$pull'] = {}
    // find by all object to remove by it
    update.$pull[key] = criteria
    // run the query
    stories.update(query, update, {}, callback)
  },
  updateStory (id, data, callback) {
    stories.update({_id: id}, {
      $set: {content: data.content}
    }, {returnUpdatedDocs: true}, callback)
  },
  saveStory (data, callback) {
    let story = {}
    if (data.content) {
      story.content = data.content
      story.acceptanceCriteries = []
      story.project = data.project
      story.startedAt = new Date()
    }
    stories.insert(story, callback)
  },
  fetchStory (id, callback) {
    stories.findOne({_id: id}, callback)
  },
  removeStory (_id, callback) {
    stories.remove({_id}, {}, callback)
  },
  fetchProjectStories (callback, where = {}) {
    stories.find(where).sort({ _id: -1 }).exec(callback)
  },
  saveProject (project, callback) {
    project.startedAt = new Date()
    projects.insert(project, callback)
  },
  fetchAllProjects (callback, where = {}) {
    projects.find(where).sort({ _id: -1 }).exec(callback)
  },
  updateProject (project, callback) {
    projects.update({_id: project.id}, {
      $set: {title: project.title}
    }, {returnUpdatedDocs: true}, callback)
  },
  removeProject (project, callback, callback2) {
    projects.remove({ _id: project._id }, {}, callback)
    stories.remove({ project: project.title }, { multi: true }, callback2)
  }
}
