const { app, BrowserWindow, Menu, shell, Tray } = require('electron')

const logo = `${__dirname}/static/logo-64.png`
let tray
// const devtools = require('electron-devtools-installer').default
// const {VUEJS_DEVTOOLS} = require('electron-devtools-installer')
// devtools(VUEJS_DEVTOOLS)
//   .then((name) => console.log(`Added Extension:  ${name}`))
//   .catch((err) => console.log('An error occurred: ', err))

let menu
let mainWindow = null

if (process.env.NODE_ENV === 'development') {
  require('electron-debug')({showDevTools: false}) // eslint-disable-line global-require
}

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') app.quit()
  // mainWindow.loadURL('https://tonicdev.com/npm/electron-devtools-installer')
})

app.on('ready', () => {
  tray = new Tray(logo)
  tray.setToolTip('Maham')

  mainWindow = new BrowserWindow({
    show: false,
    width: 1366,
    height: 728,
    icon: logo
  })

  tray.on('click', () => {
    mainWindow.isVisible() ? mainWindow.hide() : mainWindow.show()
  })

  mainWindow.loadURL('http://localhost:8080')

  mainWindow.webContents.on('did-finish-load', () => {
    mainWindow.show()
    mainWindow.focus()
  })

  mainWindow.on('closed', () => {
    mainWindow = null
  })

  if (process.env.NODE_ENV === 'development') {
    require('./desktop-menu')(shell, mainWindow, app, Menu, menu)
  }
})
