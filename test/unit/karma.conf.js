module.exports = function (config) {
  config.set({
    browsers: ['Electron'], // use electron browser
    // client: {
      // loadScriptsViaRequire: true
      // ,useIframe: false
    // },
    frameworks: ['mocha', 'sinon-chai'],
    reporters: ['spec', 'growl', 'clear-screen'], // spec reporter in terminal, growl notification in desktop
    files: ['./index.js'], // entery
    preprocessors: {
      './index.js': ['webpack', 'sourcemap', 'electron']  // webpack for build , sourcemap for error directo to source file
    },
    webpack: require('./webpack.testing.js'), // webpack config for testing preparations
    webpackMiddleware: {
      quiet: true // hide webpack logs
    }
    // ,logLevel: config.LOG_DISABLE // disable karma logs
  })
}
